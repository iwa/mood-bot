import { writeFileSync, readFileSync } from 'fs'

export interface Mood {
	emoji: string,
	name: string,
	links: string[]
}

const saveFileName = 'moods.json';
export function saveMoods() {
	writeFileSync(saveFileName, JSON.stringify(moods));
}

export function loadMoods() {
	var moodString = readFileSync(saveFileName, { encoding: 'UTF-8' });
	moods = JSON.parse(moodString);
	return moods;
}

export var moods: Mood[] = [
	{
		emoji: '😏',
		name: 'Troll',
		links: [
			'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
		]
	},
	{
		emoji: '🔥',
		name: 'Fire',
		links: [
			'https://www.youtube.com/watch?v=bmlg1HkjUjA',
			'https://www.youtube.com/watch?v=DPbvoJlBRkw',
			'https://www.youtube.com/watch?v=JJ9rEcqxM2E'
		]
	},
];



