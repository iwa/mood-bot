import Discord, { Message } from 'discord.js'
import { registerCommands } from './commands';

export const client = new Discord.Client();

export const commandPrefix = "/";

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

/*client.on('voiceStateUpdate', async (vs1, vs2) => {
	if (vs2.channel != null) {
		const connection = await vs2.channel.join();
	}
	else {
		var memberCount = vs1.channel.members.size;
		if (memberCount <= 1) {
			for (const conn of client.voice.connections.values()) {
				conn.disconnect();
			}
		}
	}
});*/


var commandList = [{
	command: 'ping',
	parameterNames: [],
	handler: (params, msg: Message, optionalParams) => msg.reply('!pong')
}];

type commandHandler = ((params, msg: Message) => any) | ((params, msg: Message, optionalParams) => any);

export function registerCommandHandler(command: string, parameterNames: string[], handler: commandHandler) {
	commandList.push({
		command,
		parameterNames,
		handler
	});
}

export function registerCommandAlias(command: string, alias: string) {
	var originalCommand = commandList.find(c => c.command == command);
	commandList.push({
		command: alias,
		parameterNames: originalCommand.parameterNames,
		handler: originalCommand.handler
	});
}

function handleCommands(msg: Message) {
	var content = msg.content;
	var foundCommand = false;
	var split = content.split(' ');
	var readCommand = split[0];

	for (const command of commandList) {
		if (commandPrefix + command.command == readCommand) {
			var namedParametersLength = command.parameterNames.length
			if (split.length >= namedParametersLength + 1) {
				var rawParameters = split.slice(1).filter(p => p.length > 0);
				var parameters = rawParameters
					.slice(0, namedParametersLength)
					.map((val, i) => [command.parameterNames[i], val]);
				var parameterObj = Object.fromEntries(parameters);

				if (rawParameters.length > namedParametersLength) {
					var optionalParameters = rawParameters.slice(namedParametersLength);
					command.handler(parameterObj, msg, optionalParameters);
				}
				else {
					command.handler(parameterObj, msg, null);
				}

			}
			else {
				msg.reply(`You are not using the command ${readCommand} correctly, use it like this: `);
				msg.reply(readCommand + ' ' + command.parameterNames.join(' '));
			}

			msg.react('👌');
			foundCommand = true;
			break;
		}
	}

	if (!foundCommand) {
		msg.reply('No command found with name ' + readCommand);
	}
}

client.on('message', async msg => {
	if (msg.author.id != client.user.id) {
		
		if (msg.content === 'hoi') {
			var newMsg = await msg.reply('jo seavus!');
		}
		else if (msg.content.startsWith(commandPrefix)) {
			handleCommands(msg);
		}
		else {
			//var newMsg = await msg.reply('sog amol hoi');
		}
	}

});

registerCommandHandler('help', [], (params, msg) => {
	var text = "";
	for (const command of commandList) {
		text += command.command + " ";
		text += command.parameterNames.map(p => '<' + p + '>').join(' ') + '\n'
	}

	const exampleEmbed = new Discord.MessageEmbed()
		.setColor('#0099ff')
		.setTitle('Mood-Bot help screen')
		.setDescription(text)

	msg.reply(exampleEmbed); 
});

registerCommands();
client.login('NzAzMDExODgzMTM2ODQzNzk2.XqIY-A.gSQY3bXHuf8Q98bj8xPRMdMp10I');